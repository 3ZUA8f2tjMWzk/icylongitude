#!/bin/bash

unset PATH

JAVA=/usr/bin/java
IVYDIR=$(/usr/bin/dirname "${0}")
ICYLONGITUDEDIR=$(/usr/bin/dirname "${IVYDIR}")
TARGET="${1}"
PUBLISH_PATTERN="${2}"
REVISION="${3}"
REPO="${4}"
IVYSETTINGSFILE=$(/bin/mktemp)
/bin/sed -e "s!ICYLONGITUDEDIR!${ICYLONGITUDEDIR}!" -e "w${IVYSETTINGSFILE}" "${IVYDIR}/ivysettings.xml"
${JAVA} -jar "${IVYDIR}"/ivy.jar -settings "${IVYSETTINGSFILE}" -ivy "${IVYDIR}"/"${TARGET}.xml" -publish "${REPO}" -publishpattern "${PUBLISH_PATTERN}" -revision "${REVISION}" -status release -overwrite
